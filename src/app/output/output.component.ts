import {Component, OnInit} from '@angular/core';
import {ConverterService, TableData} from '../converter.service';
import {BehaviorSubject} from 'rxjs';

@Component({
  selector: 'app-output',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.scss']
})
export class OutputComponent {

  tableData: BehaviorSubject<TableData> = this.converter.tableData;

  clickedHeader = '';

  constructor(private converter: ConverterService) {

  }

  onHeaderClick(e, column) {
    if (this.clickedHeader === e.target) {
      this.converter.order(column, true);
      this.clickedHeader = '';
    } else {
      this.converter.order(column, false);
      this.clickedHeader = e.target;
    }
  }

}
