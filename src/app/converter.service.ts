import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';

export interface TableData {
  headers: Array<any>;
  data: Array<Array<any>>;

}

@Injectable({
  providedIn: 'root'
})


export class ConverterService {

  tableData = new BehaviorSubject<TableData>(null);


  constructor() {
  }

  convertCSV(csvContent: string): void {
    const result = {headers: [], data: []};
    const lines = csvContent.split('\n');
    const headers = lines[0].split(';');

    lines.splice(0, 1);

    console.log({lines});

    lines.forEach((line) => {
      const row = [];
      const items = line.split(';');
      for (let i = 0; i < headers.length; i++) {
        row.push(items[i]);
      }
      result.data.push(row);

    });
    result.headers = headers;
    console.log(result);

    this.tableData.next(result);

  }

  order(column: number, desc): void {
    console.log('ja');
    let data1 = this.tableData.getValue().data;
    let sorter;

    switch (column) {
      case 3:
        sorter = (a, b) => {
          const aDate = new Date(a[3]);
          const bDate = new Date(b[3]);
          return bDate.getTime() - aDate.getTime();
        };
        break;
      case 2:
        sorter = (a, b) => {
          return parseInt(b[2], 10) - parseInt(a[2], 10);
        };
        break;
      default:
        sorter =  (a, b) => {
          if (a[column] < b[column]) {
            return -1;
          }
          if (a[column] > b[column]) {
            return 1;
          } else {
            return 0;
          }

        };
    }

    data1 = data1.sort(sorter);
    if (desc) {
      data1.reverse();
    }
    console.log({data1});
    this.tableData.next({headers: this.tableData.getValue().headers, data: data1});

  }

}
