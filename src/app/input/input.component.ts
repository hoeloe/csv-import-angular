import {Component} from '@angular/core';
import {ConverterService} from '../converter.service';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent {

  public CSVFileContent: string | ArrayBuffer;

  constructor(private converter: ConverterService) {
  }


  onFileSelect(file): void {
    const fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.CSVFileContent = fileReader.result;
    };
    fileReader.readAsText(file);
  }

  convert(): void {
    const element: HTMLTextAreaElement = document.getElementById('displayCSVData') as HTMLTextAreaElement
    if (element.value) {
      const jsonData = this.converter.convertCSV(element.value);
    } else {
      console.error('Nothing to convert');
    }
  }


}
